﻿using AutoMapper;
using NewsFeed.Data.Models;
using NewsFeed.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewsFeed.Logic.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void AutoMapperConfiguration()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<FeedsUserDTO, FeedsUser>().ForMember(app => app.News, opt => opt.Ignore());
                cfg.CreateMap<FeedsUser, FeedsUserDTO>();
                cfg.CreateMap<NewsCategoryDTO, NewsCategory>().ForMember(app => app.News, opt => opt.Ignore());
                cfg.CreateMap<NewsCategory, NewsCategoryDTO>();
                cfg.CreateMap<NewsDTO, News>().ForMember(app => app.NewsCategoryNavigation, opt => opt.Ignore())
                .ForMember(app => app.UserCreatedNavigation, opt => opt.Ignore());
                cfg.CreateMap<News, NewsDTO>();
            });
        }
    }
}
