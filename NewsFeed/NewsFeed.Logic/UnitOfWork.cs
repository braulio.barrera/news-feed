﻿using NewsFeed.Data.Models;
using NewsFeed.Domain;
using NewsFeed.Domain.Repositories;
using NewsFeed.Logic.RepositoriesImp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewsFeed.Logic
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly NewsFeedContext _context;

        private INewsRepository _newsRepository;
        private INewsCategoryRepository _newsCategoryRepository;
        private IUserFeedRepository _userFeedRepository;

        public UnitOfWork()
        {
            _context = new NewsFeedContext();
        }

        public INewsRepository NewsRepository => _newsRepository ?? (_newsRepository = new NewsRepository(_context));
        public INewsCategoryRepository NewsCategoryRepository => _newsCategoryRepository ?? (_newsCategoryRepository = new NewsCategoryRepository(_context));
        public IUserFeedRepository UserFeedRepository => _userFeedRepository ?? (_userFeedRepository = new UserFeedRepository(_context));

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
