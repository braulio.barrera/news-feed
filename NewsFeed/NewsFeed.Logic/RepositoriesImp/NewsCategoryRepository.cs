﻿using AutoMapper;
using NewsFeed.Data.Models;
using NewsFeed.Domain.DTO;
using NewsFeed.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NewsFeed.Logic.RepositoriesImp
{
    public class NewsCategoryRepository : Repository<NewsCategoryDTO, NewsCategory>, INewsCategoryRepository
    {
        private readonly NewsFeedContext _context;

        public NewsCategoryRepository(NewsFeedContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<NewsCategoryDTO> FindByName(string query)
        {
            try
            {
                var clients = Mapper.Map<IEnumerable<NewsCategory>, IEnumerable<NewsCategoryDTO>>(Set.Where(c => (c.CategoryName.Contains(query)) || string.IsNullOrEmpty(query)).OrderBy(a => a.CategoryName).ToList());
                return clients;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       
    }
}
