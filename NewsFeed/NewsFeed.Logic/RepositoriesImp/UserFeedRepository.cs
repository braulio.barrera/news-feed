﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NewsFeed.Data.Models;
using NewsFeed.Domain.DTO;
using NewsFeed.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NewsFeed.Logic.RepositoriesImp
{
    public class UserFeedRepository : Repository<FeedsUserDTO, FeedsUser>, IUserFeedRepository
    {
        private readonly NewsFeedContext _context;

        public UserFeedRepository(NewsFeedContext context) : base(context)
        {
            _context = context;
        }
        
        public async Task<FeedsUserDTO> FindByUserId(string id)
        {
            try
            {
                var users =  await Set.ToListAsync();
                var userEntity = new FeedsUser();
                foreach(var user in users)
                {
                    if(user.AspNetUserId == id)
                    {
                        userEntity = user;
                    }
                }
                return Mapper.Map<FeedsUserDTO>(userEntity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
