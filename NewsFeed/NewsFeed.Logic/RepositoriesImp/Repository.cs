﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NewsFeed.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewsFeed.Logic.RepositoriesImp
{
    public class Repository<TObjectDto, TEntity> : IRepository<TObjectDto> where TObjectDto : class
                                                                           where TEntity : class
    {
        private DbContext _context;
        private DbSet<TEntity> _set;

        internal Repository(DbContext context)
        {
            _context = context;
        }

        protected DbSet<TEntity> Set => _set ?? (_set = _context.Set<TEntity>());

        public async Task<IEnumerable<TObjectDto>> ListAsync()
        {
            try
            {
                List<TEntity> data = await Set.ToListAsync();
                var list = Mapper.Map<List<TObjectDto>>(data);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<TObjectDto> List()
        {
            try
            {
                List<TEntity> data = Set.ToList();
                var list = Mapper.Map<List<TObjectDto>>(data);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<TObjectDto>> ListAsync(CancellationToken cancellationToken)
        {
            try
            {
                List<TEntity> data = await Set.ToListAsync(cancellationToken);
                var list = Mapper.Map<List<TObjectDto>>(data);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TObjectDto FindById(object id)
        {
            try
            {
                var objectDto = Mapper.Map<TEntity, TObjectDto>(Set.Find(id));
                return objectDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<TObjectDto> FindByIdAsync(object id)
        {
            try
            {
                var objectEntity = await Set.FindAsync(id);
                return Mapper.Map<TObjectDto>(objectEntity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TObjectDto> FindByIdAsync(CancellationToken cancellationToken, object id)
        {
            try
            {
                var objectEntity = await Set.FindAsync(cancellationToken, id);
                var objectDto = Mapper.Map<TObjectDto>(objectEntity);

                return objectDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IQueryable<TObjectDto> Query()
        {
            return _context.Set<TObjectDto>().AsQueryable();
        }

        public void Add(TObjectDto objectDto)
        {
            try
            {
                Set.Add(Mapper.Map<TObjectDto, TEntity>(objectDto));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(TObjectDto objectDto)
        {
            try
            {
                var dto = Mapper.Map<TObjectDto, TEntity>(objectDto);
                Set.Attach(dto);
                _context.Entry(dto).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Remove(object id)
        {
            try
            {
                var dto = Set.Find(id);
                Set.Remove(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
