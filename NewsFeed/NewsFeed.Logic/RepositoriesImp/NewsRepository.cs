﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NewsFeed.Data.Models;
using NewsFeed.Domain.DTO;
using NewsFeed.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewsFeed.Logic.RepositoriesImp
{
    public class NewsRepository : Repository<NewsDTO, News>, INewsRepository
    {
        private readonly NewsFeedContext _context;

        public NewsRepository(NewsFeedContext context) : base(context)
        {
            _context = context;
        }

        public void Subscribe(int id, string subscriptor)
        {
            try
            {
                var objectDto = Set.Find(id);
                var subscriptions = objectDto.UsersSubscribed;
                objectDto.UsersSubscribed = subscriptions + subscriptor;
                Set.Attach(objectDto);
                _context.Entry(objectDto).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
