﻿using NewsFeed.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewsFeed.Domain
{
    public interface IUnitOfWork : IDisposable
    {
        INewsRepository NewsRepository { get; }
        INewsCategoryRepository NewsCategoryRepository { get; }
        IUserFeedRepository UserFeedRepository { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

    }
}
