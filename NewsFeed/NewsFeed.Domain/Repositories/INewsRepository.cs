﻿using NewsFeed.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewsFeed.Domain.Repositories
{
    public interface INewsRepository : IRepository<NewsDTO>
    {
        void Subscribe(int id, string subscriptor);
    }
}
