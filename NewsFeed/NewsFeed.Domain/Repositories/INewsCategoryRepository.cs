﻿using NewsFeed.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewsFeed.Domain.Repositories
{
    public interface INewsCategoryRepository : IRepository<NewsCategoryDTO>
    {
        IEnumerable<NewsCategoryDTO> FindByName(string query);
    }
}
