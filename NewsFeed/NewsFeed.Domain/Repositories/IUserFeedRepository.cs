﻿using NewsFeed.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace NewsFeed.Domain.Repositories
{
    public interface IUserFeedRepository : IRepository<FeedsUserDTO>
    {
        Task<FeedsUserDTO> FindByUserId(string Id);
    }
}
