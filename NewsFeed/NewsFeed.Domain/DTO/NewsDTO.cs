﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NewsFeed.Domain.DTO
{
    public class NewsDTO
    {
        public int NewsId { get; set; }
        [Display(Name = "Title")]
        public string NewTitle { get; set; }
        [Display(Name = "Description")]
        public string NewDescription { get; set; }
        [Display(Name = "Created By")]
        public int UserCreated { get; set; }
        [Display(Name = "Users Subscribed")]
        public string UsersSubscribed { get; set; }
        [Display(Name = "Category")]
        public int NewsCategory { get; set; }
        public string NewsCategoryName { get; set; }
    }
}
