﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsFeed.Domain.DTO
{
    public class FeedsUserDTO
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AspNetUserId { get; set; }
    }
}
