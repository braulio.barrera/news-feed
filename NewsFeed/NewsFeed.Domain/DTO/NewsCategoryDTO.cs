﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsFeed.Domain.DTO
{
    public class NewsCategoryDTO
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
