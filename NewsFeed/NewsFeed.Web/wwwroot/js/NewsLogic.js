﻿$(document).ready(function () {
    GetCategories();
    $("#newsTable").DataTable({
        "language": {
            "info": 'Page ' + ' _PAGE_ ' + 'of ' + ' _PAGES_',
            "next": ' Next ',
            "previous": 'Previous ',
            "lengthMenu": 'Show' + " _MENU_ " + 'rows',
            "search": "Search:",

            paginate: {
                previous: 'Previous ',
                next: ' Next '
            }
        }
    });
})

function GetCategories() {
    var BaseUrl = $('#BaseCategoriesUrl').val();
    $('.js-example-responsive').select2({
        theme: "classic",
        width: 'resolve',
        ajax: {
            url: BaseUrl + "/GetCategories",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    term: params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.length
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup },
        minimumInputLength: 1
    });
}