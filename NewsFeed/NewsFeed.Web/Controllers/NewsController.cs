﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsFeed.Domain;
using NewsFeed.Domain.DTO;
using Microsoft.AspNetCore.Identity;

namespace NewsFeed.Web.Controllers
{
    public class NewsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<IdentityUser> _userManager;

        public NewsController(IUnitOfWork unitOfWork, UserManager<IdentityUser> userManager)
        {
            this.unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public async Task<IEnumerable<NewsCategoryDTO>> GetNewsCategories()
        {
            return await unitOfWork.NewsCategoryRepository.ListAsync();
        }

        public async Task<IEnumerable<NewsDTO>> GetNews()
        {
            return await unitOfWork.NewsRepository.ListAsync();
        }

        // GET: News
        public async Task<ActionResult> Index()
        {
            try
            {
                var userId =  await GetUserId();
                var newsList = await GetNews();
                var categories = await GetNewsCategories();

                var model = newsList.Where(u => u.UserCreated == userId);

                var list = from nw in model
                           join cat in categories on nw.NewsCategory equals cat.CategoryId
                           select new NewsDTO
                           {
                               NewsId = nw.NewsId,
                               NewTitle = nw.NewTitle,
                               NewDescription = nw.NewDescription,
                               NewsCategory = nw.NewsCategory,
                               NewsCategoryName = cat.CategoryName,
                               UserCreated = nw.UserCreated,
                               UsersSubscribed = nw.UsersSubscribed
                           };

                ViewBag.Subscriptions = list.Where(m => m.UsersSubscribed != null).Select(m => m.UsersSubscribed).ToString().Split(",").Count();
                return View(list);
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public async Task<IEnumerable<FeedsUserDTO>> GetFeedUsers()
        {
            return await unitOfWork.UserFeedRepository.ListAsync();
        }

        // GET: News/Details/5
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var news = await unitOfWork.NewsRepository.FindByIdAsync(id);
                var users = await GetFeedUsers();
                ViewBag.createdBy = users.Where(u => u.UserId == news.UserCreated).Select(u => u.FirstName).Single();
                if (news != null)
                {
                    return View(news);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        public JsonResult GetCategories(string term, string page)
        {
            var result = unitOfWork.NewsCategoryRepository.FindByName(term).Select(cat => new { id = cat.CategoryId, text = cat.CategoryName }).ToList();
            return Json(result);
        }

        // GET: News/Create
        public ActionResult Create()
        {
            List<NewsCategoryDTO> newsCategories = new List<NewsCategoryDTO>();
            newsCategories = (from cat in unitOfWork.NewsCategoryRepository.List()
                              select cat).ToList();

            ViewBag.CategoryList = newsCategories;
            return View();
        }

        // POST: News/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(NewsDTO news)
        {
            try
            {                
                news.UserCreated = await GetUserId();
                unitOfWork.NewsRepository.Add(news);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: News/Edit/5
        public ActionResult Edit(int id)
        {
            var model = unitOfWork.NewsRepository.FindById(id);
            List<NewsCategoryDTO> newsCategories = new List<NewsCategoryDTO>();
            newsCategories = (from cat in unitOfWork.NewsCategoryRepository.List()
                              select cat).ToList();

            ViewBag.CategoryList = newsCategories;
            return View(model);
        }

        // POST: News/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, NewsDTO news)
        {
            try
            {
                news.UserCreated = await GetUserId();
                unitOfWork.NewsRepository.Update(news);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: News/Delete/5
        public ActionResult Delete(int id)
        {
            var model = unitOfWork.NewsRepository.FindById(id);
            return View(model);
        }

        // POST: News/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, NewsDTO news)
        {
            try
            {
                unitOfWork.NewsRepository.Remove(id);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public async Task<int> GetUserId()
        {
            var userIdentity = _userManager.GetUserId(User);
            var userId = await unitOfWork.UserFeedRepository.FindByUserId(userIdentity);
            return userId.UserId;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Subscribe(int id)
        {
            try
            {
                var subscriptor = $"{_userManager.GetUserName(User)},";
                unitOfWork.NewsRepository.Subscribe(id, subscriptor);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction("Index", "Home"); 
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}