﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsFeed.Domain;
using NewsFeed.Domain.DTO;

namespace NewsFeed.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Category
        public async Task<ActionResult> Index()
        {
            try
            {
                var model = await unitOfWork.NewsCategoryRepository.ListAsync();
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: Category/Details/5
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var category = await unitOfWork.NewsCategoryRepository.FindByIdAsync(id);
                if (category == null)
                {
                    return View(category);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return View();
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(NewsCategoryDTO category)
        {
            try
            {
                unitOfWork.NewsCategoryRepository.Add(category);
                await unitOfWork.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
            var model = unitOfWork.NewsCategoryRepository.FindById(id);
            return View(model);
        }

        // POST: Category/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, NewsCategoryDTO category)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }
            try
            {
                unitOfWork.NewsCategoryRepository.Update(category);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            var model = unitOfWork.NewsCategoryRepository.FindById(id);
            return View(model);
        }

        // POST: Category/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, NewsCategoryDTO category)
        {
            try
            {
                unitOfWork.NewsCategoryRepository.Remove(id);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}