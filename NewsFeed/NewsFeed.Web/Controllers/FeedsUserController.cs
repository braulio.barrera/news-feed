﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsFeed.Domain;
using NewsFeed.Domain.DTO;

namespace NewsFeed.Web.Controllers
{
    public class FeedsUserController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public FeedsUserController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        // GET: FeedsUser
        public async Task<ActionResult> Index()
        {
            try
            {
                var model = await unitOfWork.UserFeedRepository.ListAsync();
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: FeedsUser/Details/5
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var user = await unitOfWork.UserFeedRepository.FindByIdAsync(id);
                if (user != null)
                {
                    return View(user);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        // GET: FeedsUser/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FeedsUser/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FeedsUserDTO user)
        {
            try
            {
                unitOfWork.UserFeedRepository.Add(user);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FeedsUser/Edit/5
        public ActionResult Edit(int id)
        {
            var model = unitOfWork.UserFeedRepository.FindById(id);
            return View(model);
        }

        // POST: FeedsUser/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, FeedsUserDTO user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }
            try
            {
                unitOfWork.UserFeedRepository.Update(user);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FeedsUser/Delete/5
        public ActionResult Delete(int id)
        {
            var model = unitOfWork.UserFeedRepository.FindById(id);
            return View(model);
        }

        // POST: FeedsUser/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FeedsUserDTO user)
        {
            try
            {
                unitOfWork.UserFeedRepository.Remove(id);
                await unitOfWork.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}