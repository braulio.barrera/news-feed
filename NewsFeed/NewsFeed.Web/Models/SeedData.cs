﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NewsFeed.Data.Models;
using System;
using System.Linq;

namespace NewsFeed.Web.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using(var context = new NewsFeedContext(
                serviceProvider.GetRequiredService<DbContextOptions<NewsFeedContext>>()))
            {
                if(context.News.Any() || context.NewsCategory.Any())
                {
                    return;
                }

                context.NewsCategory.AddRange(
                    new NewsCategory
                    {
                        CategoryName = "Development"
                    },
                    new NewsCategory
                    {
                        CategoryName = "Q&A"
                    },
                    new NewsCategory
                    {
                        CategoryName = "Machine Learning"
                    },
                    new NewsCategory
                    {
                        CategoryName = "Science"
                    }
                    );
                context.SaveChanges();
            }
        }
    }
}
