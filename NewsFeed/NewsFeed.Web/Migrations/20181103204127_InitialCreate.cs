﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NewsFeed.Web.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FeedsUser",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    AspNetUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedsUser", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "NewsCategory",
                columns: table => new
                {
                    CategoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsCategory", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    NewsId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NewTitle = table.Column<string>(nullable: true),
                    NewDescription = table.Column<string>(nullable: true),
                    UserCreated = table.Column<int>(nullable: false),
                    UsersSubscribed = table.Column<string>(nullable: true),
                    NewsCategory = table.Column<int>(nullable: false),
                    NewsCategoryNavigationCategoryId = table.Column<int>(nullable: true),
                    UserCreatedNavigationUserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.NewsId);
                    table.ForeignKey(
                        name: "FK_News_NewsCategory_NewsCategoryNavigationCategoryId",
                        column: x => x.NewsCategoryNavigationCategoryId,
                        principalTable: "NewsCategory",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_News_FeedsUser_UserCreatedNavigationUserId",
                        column: x => x.UserCreatedNavigationUserId,
                        principalTable: "FeedsUser",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_News_NewsCategoryNavigationCategoryId",
                table: "News",
                column: "NewsCategoryNavigationCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_News_UserCreatedNavigationUserId",
                table: "News",
                column: "UserCreatedNavigationUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "NewsCategory");

            migrationBuilder.DropTable(
                name: "FeedsUser");
        }
    }
}
