﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NewsFeed.Data.Models
{
    public partial class News
    {
        [Key]
        public int NewsId { get; set; }        
        public string NewTitle { get; set; }
        public string NewDescription { get; set; }
        public int UserCreated { get; set; }
        public string UsersSubscribed { get; set; }
        public int NewsCategory { get; set; }

        public NewsCategory NewsCategoryNavigation { get; set; }
        public FeedsUser UserCreatedNavigation { get; set; }
    }
}
