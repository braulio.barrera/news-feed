﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NewsFeed.Data.Models
{
    public partial class NewsFeedContext : DbContext
    {
        public NewsFeedContext()
        {
        }

        public NewsFeedContext(DbContextOptions<NewsFeedContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FeedsUser> FeedsUser { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NewsCategory> NewsCategory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=NewsFeed;Trusted_Connection=True;MultipleActiveResultSets=true");
            }
        }
        
    }
}
