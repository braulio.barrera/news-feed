﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NewsFeed.Data.Models
{
    public partial class FeedsUser
    {
        public FeedsUser()
        {
            News = new HashSet<News>();
        }

        [Key]
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AspNetUserId { get; set; }

        public ICollection<News> News { get; set; }
    }
}
