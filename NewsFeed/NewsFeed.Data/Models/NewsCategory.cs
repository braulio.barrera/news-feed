﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NewsFeed.Data.Models
{
    public partial class NewsCategory
    {
        public NewsCategory()
        {
            News = new HashSet<News>();
        }
        [Key]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public ICollection<News> News { get; set; }
    }
}
