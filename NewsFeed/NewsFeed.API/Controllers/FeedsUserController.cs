﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsFeed.Domain;
using NewsFeed.Domain.DTO;

namespace NewsFeed.API.Controllers
{
    [Route("api/user-feeds")]
    [ApiController]
    public class FeedsUserController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public FeedsUserController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: api/FeedsUser
        [HttpGet]
        public async Task<IEnumerable<FeedsUserDTO>> Get()
        {
            try
            {
                return await unitOfWork.UserFeedRepository.ListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/FeedsUser/5
        [HttpGet("{id}", Name = "GetUsers")]
        public async Task<IActionResult> Get(int id)
        {
            var user = await unitOfWork.UserFeedRepository.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        // POST: api/FeedsUser
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] FeedsUserDTO user)
        {
            if (user == null)
            {
                return BadRequest();
            }

            unitOfWork.UserFeedRepository.Add(user);
            await unitOfWork.SaveChangesAsync();

            return Created($"user/{user.UserId}", user);
        }

        // PUT: api/FeedsUser/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] FeedsUserDTO user)
        {
            if (user == null || user.UserId != id)
            {
                return BadRequest();
            }

            unitOfWork.UserFeedRepository.Update(user);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await unitOfWork.UserFeedRepository.FindByIdAsync(id);
            if (user == null || user.UserId != id)
            {
                return BadRequest();
            }

            unitOfWork.UserFeedRepository.Remove(user);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }
    }
}
