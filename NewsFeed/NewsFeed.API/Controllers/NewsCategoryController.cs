﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsFeed.Domain;
using NewsFeed.Domain.DTO;

namespace NewsFeed.API.Controllers
{
    [Route("api/news-category")]
    [ApiController]
    public class NewsCategoryController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public NewsCategoryController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IEnumerable<NewsCategoryDTO>> Get()
        {
            try
            {
                return await unitOfWork.NewsCategoryRepository.ListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id}", Name = "GetCategories")]
        public async Task<IActionResult> Get(int id)
        {
            var category = await unitOfWork.NewsCategoryRepository.FindByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            return Ok(category);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NewsCategoryDTO category)
        {
            if (category == null)
            {
                return BadRequest();
            }

            unitOfWork.NewsCategoryRepository.Add(category);
            await unitOfWork.SaveChangesAsync();

            return Created($"category/{category.CategoryId}", category);

        }



        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] NewsCategoryDTO category)
        {
            if (category == null || category.CategoryId != id)
            {
                return BadRequest();
            }

            unitOfWork.NewsCategoryRepository.Update(category);
            await unitOfWork.SaveChangesAsync();

            return NoContent();

        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var category = await unitOfWork.NewsCategoryRepository.FindByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            unitOfWork.NewsCategoryRepository.Remove(id);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }
    }
}