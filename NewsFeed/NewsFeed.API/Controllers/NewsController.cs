﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsFeed.Domain;
using NewsFeed.Domain.DTO;

namespace NewsFeed.API.Controllers
{
    [Route("api/news")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public NewsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        
        [HttpGet]
        public async Task<IEnumerable<NewsDTO>> Get()
        {
            try
            {
                return await unitOfWork.NewsRepository.ListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        [HttpGet("{id}", Name = "GetNews")]
        public async Task<IActionResult> Get(int id)
        {
            var news = await unitOfWork.NewsRepository.FindByIdAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            return Ok(news);
        }

        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NewsDTO news)
        {
            if (news == null)
            {
                return BadRequest();
            }

            unitOfWork.NewsRepository.Add(news);
            await unitOfWork.SaveChangesAsync();

            return Created($"news/{news.NewsId}", news);
        }

        
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] NewsDTO news)
        {
            if (news == null || news.NewsId != id)
            {
                return BadRequest();
            }

            unitOfWork.NewsRepository.Update(news);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }

        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var news = await unitOfWork.NewsRepository.FindByIdAsync(id);
            if (news == null || news.NewsId != id)
            {
                return BadRequest();
            }

            unitOfWork.NewsRepository.Remove(news);
            await unitOfWork.SaveChangesAsync();

            return NoContent();
        }
    }
}
