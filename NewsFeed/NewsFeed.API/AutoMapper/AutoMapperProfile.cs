﻿using AutoMapper;
using NewsFeed.Logic.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsFeed.API.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile() : this("MapperProfile")
        {

        }

        public AutoMapperProfile(string profileName) : base(profileName)
        {
            AutoMapperConfig.AutoMapperConfiguration();
        }
    }
}
